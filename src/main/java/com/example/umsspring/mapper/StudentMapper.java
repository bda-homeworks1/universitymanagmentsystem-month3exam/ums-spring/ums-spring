package com.example.umsspring.mapper;

import com.example.umsspring.model.dto.request.CreateStudentRequest;
import com.example.umsspring.model.dto.response.StudentResponse;
import com.example.umsspring.model.entity.StudentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StudentMapper {

    @Mapping(target = "id", ignore = true)
    StudentEntity mapStudentRequestToStudentEntity(CreateStudentRequest createStudentRequest);

    StudentResponse mapStudentEntityToStudentResponse(StudentEntity studentEntity);
}
