package com.example.umsspring.mapper;

import com.example.umsspring.model.dto.request.CreateAdminRequest;
import com.example.umsspring.model.dto.response.AdminResponse;
import com.example.umsspring.model.entity.AdminEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AdminMapper {

        @Mapping(target = "id", ignore = true)
        AdminEntity mapAdminRequestToAdminEntity(CreateAdminRequest adminRequest);

        AdminResponse mapAdminEntityToAdminResponse(AdminEntity authorEntity);
}
