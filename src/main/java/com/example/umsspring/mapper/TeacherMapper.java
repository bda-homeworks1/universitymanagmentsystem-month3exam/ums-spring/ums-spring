package com.example.umsspring.mapper;

import com.example.umsspring.model.dto.request.CreateTeacherRequest;
import com.example.umsspring.model.dto.response.TeacherResponse;
import com.example.umsspring.model.entity.TeacherEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface TeacherMapper {

    @Mapping(target = "id", ignore = true)
    TeacherEntity mapTeacherRequestToTeacherEntity(CreateTeacherRequest createTeacherRequest);

    TeacherResponse mapTeacherEntityToTeacherResponse(TeacherEntity teacherEntity);
}

