package com.example.umsspring.controller;

import com.example.umsspring.model.dto.response.StudentResponse;
import com.example.umsspring.service.StudentServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class StudentController {

    private final StudentServices studentServices;

    @Autowired
    public StudentController(StudentServices studentServices) {
        this.studentServices = studentServices;
    }

    @GetMapping("/api/student")
    public List<StudentResponse> getStudents(){
        System.out.println(studentServices.getStudents());
        return studentServices.getStudents();
    }


}
