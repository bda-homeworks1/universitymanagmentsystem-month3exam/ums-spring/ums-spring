package com.example.umsspring.controller;

import com.example.umsspring.model.entity.GroupDataEntity;
import com.example.umsspring.service.GroupDataServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class GroupDataController {

    private final GroupDataServices groupDataServices;
    @Autowired
    public GroupDataController(GroupDataServices groupDataServices) {
        this.groupDataServices = groupDataServices;
    }

    @GetMapping("/groupdata")
    public List<GroupDataEntity> getGroupDatas(){
        return groupDataServices.getGroupDatas();
    }

    @PostMapping("/admin/add-teacher")
    public void addTeacherToGroup(){
        groupDataServices.createGroupData();
    }

}
