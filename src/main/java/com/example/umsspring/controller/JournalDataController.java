package com.example.umsspring.controller;

import com.example.umsspring.model.entity.JournalDataEntity;
import com.example.umsspring.service.JournalDataServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class JournalDataController {

    private final JournalDataServices journalDataServices;

    @Autowired
    public JournalDataController(JournalDataServices journalDataServices) {
        this.journalDataServices = journalDataServices;
    }

    @GetMapping("/journal-data")
    public List<JournalDataEntity> getJournalDatas(){
      return journalDataServices.getJournalDatas();
    }

    @PostMapping("/add-attendance")
    public void addAttendance(){
        journalDataServices.createJournalData();
    }
}
