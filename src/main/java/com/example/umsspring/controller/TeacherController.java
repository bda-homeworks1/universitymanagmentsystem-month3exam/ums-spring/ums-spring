package com.example.umsspring.controller;

import com.example.umsspring.model.entity.TeacherEntity;
import com.example.umsspring.service.TeacherServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class TeacherController {

    private final TeacherServices teacherServices;

    //Xais edirem servicelerde hemcinin constructor injection isledin!
    @Autowired
    public TeacherController(TeacherServices teacherServices) {
        this.teacherServices = teacherServices;
    }

    @GetMapping("/api/teacher")
    public List<TeacherEntity> getTeachers() {
//        return "pox";
        return teacherServices.getTeachers();
    }



}
