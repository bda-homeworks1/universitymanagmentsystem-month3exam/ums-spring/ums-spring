package com.example.umsspring.controller;


import com.example.umsspring.model.entity.GroupEntity;
import com.example.umsspring.service.GroupServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class GroupController {

    private final GroupServices groupServices;

    @Autowired
    public GroupController(GroupServices groupServices) {
        this.groupServices = groupServices;
    }

    @GetMapping("/groups")
    public List<GroupEntity> getGroups(){
        return groupServices.getGroups();
    }



}
