package com.example.umsspring.controller;

import com.example.umsspring.model.dto.request.CreateAdminRequest;
import com.example.umsspring.model.dto.request.CreateGroupRequest;
import com.example.umsspring.model.dto.request.CreateStudentRequest;
import com.example.umsspring.model.dto.request.CreateTeacherRequest;
import com.example.umsspring.model.entity.AdminEntity;
import com.example.umsspring.service.AdminServices;
import com.example.umsspring.service.GroupServices;
import com.example.umsspring.service.StudentServices;
import com.example.umsspring.service.TeacherServices;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api")
@RestController
@CrossOrigin
public class AdminController {

    private final AdminServices adminServices;
    private final TeacherServices teacherServices;
    private final StudentServices studentServices;

    private final GroupServices groupServices;

    public AdminController(AdminServices adminServices, TeacherServices teacherServices, StudentServices studentServices, GroupServices groupServices) {
        this.adminServices = adminServices;
        this.teacherServices = teacherServices;
        this.studentServices = studentServices;
        this.groupServices = groupServices;
    }

    @GetMapping("/admin")
    public List<AdminEntity> getAdmins(){

        System.out.println("get adminsss");
        return adminServices.getAdmins();
    }

    @PostMapping("/admin")
    public void createAdmin(@RequestBody CreateAdminRequest createAdminRequest){
        adminServices.createAdmin(createAdminRequest);
    }

    @PostMapping("/teacher")
    public void createTeacher(@RequestBody CreateTeacherRequest createTeacherRequest) {
        teacherServices.createTeacher(createTeacherRequest);
    }

    @PostMapping("/student")
    public void createStudent(@RequestBody CreateStudentRequest createStudentRequest){
        studentServices.createStudent(createStudentRequest);
    }

    @PostMapping("/group")
    public void createGroup(@RequestBody CreateGroupRequest createGroupRequest){
        groupServices.createGroup(createGroupRequest);
    }

}
