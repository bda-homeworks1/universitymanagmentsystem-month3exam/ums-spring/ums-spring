package com.example.umsspring.controller;

import com.example.umsspring.model.entity.JournalEntity;
import com.example.umsspring.service.JournalServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class JournalController {

    private final JournalServices journalServices;

    @Autowired
    public JournalController(JournalServices journalServices) {
        this.journalServices = journalServices;
    }

    @GetMapping("/journals")
    public List<JournalEntity> getJournals(){
        return journalServices.getJournals();
    }

    @PostMapping("/create-journal")
    public void createJournal(){
        journalServices.createJournal();
    }
}
