package com.example.umsspring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping
public class CommonController {

    @GetMapping("/info")
    public RedirectView redirectViewToYoutubeChannel() {
        String youtubeChannelUrl = "https://www.youtube.com/@Games-ur5ot";
        return new RedirectView(youtubeChannelUrl);
    }

    @GetMapping("/admin")
    public RedirectView redirectYoutubeChannel() {
        String youtubeVideoUrl = "https://www.youtube.com/watch?v=MddkHGMmz-0";
        return new RedirectView(youtubeVideoUrl);
    }
}
