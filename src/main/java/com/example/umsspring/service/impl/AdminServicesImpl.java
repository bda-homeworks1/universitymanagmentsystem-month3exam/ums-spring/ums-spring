package com.example.umsspring.service.impl;


import com.example.umsspring.mapper.AdminMapper;
import com.example.umsspring.model.dto.request.CreateAdminRequest;
import com.example.umsspring.model.entity.AdminEntity;
import com.example.umsspring.repository.AdminRepository;
import com.example.umsspring.repository.UserRepository;
import com.example.umsspring.service.AdminServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServicesImpl implements AdminServices {

    private final AdminMapper adminMapper;

    private final AdminRepository adminRepository;
    private final UserRepository userRepository;



    @Autowired
    public AdminServicesImpl(AdminMapper adminMapper, AdminRepository adminRepository, UserRepository userRepository) {
        this.adminMapper = adminMapper;
        this.adminRepository = adminRepository;
        this.userRepository = userRepository;
    }

    public void createAdmin(CreateAdminRequest adminRequest){
        AdminEntity admin = adminMapper.mapAdminRequestToAdminEntity(adminRequest);
        adminRepository.save(admin);
    }

    public List<AdminEntity> getAdmins(){
        return adminRepository.findAll();
    }
}
