package com.example.umsspring.service.impl;



import com.example.umsspring.model.dto.request.CreateGroupRequest;
import com.example.umsspring.model.entity.GroupEntity;
import com.example.umsspring.repository.GroupRepository;
import com.example.umsspring.service.GroupServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServicesImpl implements GroupServices {


    private final GroupRepository groupRepository;

    @Autowired
    public GroupServicesImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public void createGroup(CreateGroupRequest createGroupRequest){


        GroupEntity group = new GroupEntity(null, createGroupRequest.getName());
        groupRepository.save(group);
    }

    public List<GroupEntity> getGroups(){
        return  groupRepository.findAll();
    }
}
