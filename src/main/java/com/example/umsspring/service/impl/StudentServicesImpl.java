package com.example.umsspring.service.impl;


import com.example.umsspring.mapper.StudentMapper;
import com.example.umsspring.model.dto.request.CreateStudentRequest;
import com.example.umsspring.model.dto.request.RegistrationRequest;
import com.example.umsspring.model.dto.response.StudentResponse;
import com.example.umsspring.model.entity.GroupEntity;
import com.example.umsspring.model.entity.StudentEntity;
import com.example.umsspring.repository.GroupRepository;
import com.example.umsspring.repository.StudentRepository;
import com.example.umsspring.service.StudentServices;

import com.example.umsspring.service.security.impl.AuthService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class StudentServicesImpl implements StudentServices {

    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

    private final StudentMapper studentMapper;
    private final AuthService authService;

    public StudentServicesImpl(StudentRepository studentRepository, GroupRepository groupRepository, StudentMapper studentMapper, AuthService authService) {
        this.studentRepository = studentRepository;
        this.groupRepository = groupRepository;
        this.studentMapper = studentMapper;
        this.authService = authService;
    }

    public List<StudentResponse> getStudents(){
        List<StudentResponse> studentResponses =  new ArrayList<>();

        List<StudentEntity> studentEntity =  studentRepository.findAll();
        for (StudentEntity item: studentEntity
        ) {

            StudentResponse response = new StudentResponse();
            response.setName(item.getName());
            response.setSurname(item.getSurname());
            response.setUsername(item.getUsername());
            response.setPassword(item.getPassword());
            response.setEmail(item.getEmail());
            response.setRoleId(item.getRoleId());
            response.setId(item.getId());
            response.setGroupName(item.getGroup().getName());

            studentResponses.add(response);
        }


        return studentResponses;
    }


    public void createStudent(CreateStudentRequest createStudentRequest) {

        GroupEntity group = new GroupEntity();
        group.setId(createStudentRequest.getGroupId());
        Optional<GroupEntity> optionalGroup = groupRepository.findById(createStudentRequest.getGroupId());
        String groupName = optionalGroup.stream().map(GroupEntity::getName).toString();
        group.setName(groupName);

        StudentEntity student = studentMapper.mapStudentRequestToStudentEntity(createStudentRequest);
        student.setGroup(group);

        RegistrationRequest request = new RegistrationRequest(createStudentRequest.getName(),
                createStudentRequest.getSurname(), createStudentRequest.getUsername(),
                createStudentRequest.getEmail(), createStudentRequest.getPassword(),
                "STUDENT",25L);

        authService.registration(request);

        studentRepository.save(student);
    }


}
