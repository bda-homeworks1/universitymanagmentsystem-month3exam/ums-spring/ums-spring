package com.example.umsspring.service.impl;

import com.example.umsspring.model.entity.GroupEntity;
import com.example.umsspring.model.entity.JournalEntity;
import com.example.umsspring.repository.GroupRepository;
import com.example.umsspring.repository.JournalRepository;
import com.example.umsspring.service.JournalServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class JournalServicesImpl implements JournalServices {
    static Scanner scanner = new Scanner(System.in);

    //Constructor injection vacibdir! Field injection istifade elemek olmaz!
    private final JournalRepository journalRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public JournalServicesImpl(JournalRepository journalRepository, GroupRepository groupRepository) {
        this.journalRepository = journalRepository;
        this.groupRepository = groupRepository;
    }

    public void createJournal(){
        System.out.println("-------------------------");
        System.out.println("Jurnal elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("Hansi qrupun jurnali olacaq?");
        Long groupId = scanner.nextLong();

        GroupEntity group = new GroupEntity();
        group.setId(groupId);

        Optional<GroupEntity> optionalGroup = groupRepository.findById(groupId);
        String groupName = optionalGroup.stream().map(item-> item.getName()).toString();
        group.setName(groupName);

        JournalEntity journal = new JournalEntity(null, group);
        journalRepository.save(journal);
    }

    public List<JournalEntity> getJournals(){
        return journalRepository.findAll();
    }
}
