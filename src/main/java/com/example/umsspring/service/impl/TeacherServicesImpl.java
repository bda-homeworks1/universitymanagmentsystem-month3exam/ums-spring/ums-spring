package com.example.umsspring.service.impl;

import com.example.umsspring.mapper.TeacherMapper;
import com.example.umsspring.model.dto.request.CreateTeacherRequest;
import com.example.umsspring.model.dto.request.RegistrationRequest;
import com.example.umsspring.model.entity.TeacherEntity;
import com.example.umsspring.repository.TeacherRepository;
import com.example.umsspring.service.TeacherServices;
import com.example.umsspring.service.security.impl.AuthService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServicesImpl implements TeacherServices {

    private final TeacherMapper teacherMapper;
    private final TeacherRepository teacherRepository;
    private final AuthService authService;

    public TeacherServicesImpl(TeacherMapper teacherMapper, TeacherRepository teacherRepository, AuthService authService) {
        this.teacherMapper = teacherMapper;
        this.teacherRepository = teacherRepository;
        this.authService = authService;
    }

    public void createTeacher(CreateTeacherRequest createTeacherRequest){
        TeacherEntity teacher = teacherMapper.mapTeacherRequestToTeacherEntity(createTeacherRequest) ;
        RegistrationRequest request = new RegistrationRequest(createTeacherRequest.getName(),createTeacherRequest.getSurname(),
                createTeacherRequest.getUsername(), createTeacherRequest.getEmail(),createTeacherRequest.getPassword(),
                "TEACHER", 25L);
        authService.registration(request);
        teacherRepository.save(teacher);
    }

    public List<TeacherEntity> getTeachers(){
        return teacherRepository.findAll();
    }
}
