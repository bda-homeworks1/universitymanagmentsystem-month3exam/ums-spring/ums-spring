package com.example.umsspring.service;

import com.example.umsspring.model.dto.request.CreateGroupRequest;
import com.example.umsspring.model.entity.GroupEntity;

import java.util.List;

public interface GroupServices {
    void createGroup(CreateGroupRequest createGroupRequest);
    List<GroupEntity> getGroups();
}
