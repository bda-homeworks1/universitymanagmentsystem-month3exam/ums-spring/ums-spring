package com.example.umsspring.service;

import com.example.umsspring.model.dto.request.CreateTeacherRequest;
import com.example.umsspring.model.entity.TeacherEntity;

import java.util.List;

public interface TeacherServices {
    void createTeacher(CreateTeacherRequest createTeacherRequest);
    List<TeacherEntity> getTeachers();
}
