package com.example.umsspring.service;

import com.example.umsspring.model.entity.JournalEntity;

import java.util.List;

public interface JournalServices {
    void createJournal();
    List<JournalEntity> getJournals();
}
