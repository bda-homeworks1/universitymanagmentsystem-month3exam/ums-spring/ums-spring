package com.example.umsspring.service;

import com.example.umsspring.model.dto.request.CreateStudentRequest;
import com.example.umsspring.model.dto.response.StudentResponse;

import java.util.List;

public interface StudentServices {
    void createStudent(CreateStudentRequest createStudentRequest);
    List<StudentResponse> getStudents();
}