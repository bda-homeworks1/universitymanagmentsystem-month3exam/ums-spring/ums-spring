package com.example.umsspring.service;

import com.example.umsspring.model.entity.GroupDataEntity;

import java.util.List;

public interface GroupDataServices {
    void createGroupData();
    List<GroupDataEntity> getGroupDatas();
}
