package com.example.umsspring.service;

import com.example.umsspring.model.dto.request.CreateAdminRequest;
import com.example.umsspring.model.entity.AdminEntity;

import java.util.List;

public interface AdminServices {

    void createAdmin(CreateAdminRequest adminRequest);
    List<AdminEntity> getAdmins();
}
