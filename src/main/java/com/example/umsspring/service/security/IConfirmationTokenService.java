package com.example.umsspring.service.security;


import com.example.umsspring.model.dto.response.ResponseDto;
import com.example.umsspring.model.entity.ConfirmationTokenEntity;

public interface IConfirmationTokenService {
    ResponseDto save(ConfirmationTokenEntity confirmationTokenEntity);
    ConfirmationTokenEntity getTokenByUUID(String uuid);
}