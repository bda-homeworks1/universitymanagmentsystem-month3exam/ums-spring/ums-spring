package com.example.umsspring.service.security;

import com.example.umsspring.model.dto.request.AuthenticationRequest;
import com.example.umsspring.model.dto.request.RegistrationRequest;
import com.example.umsspring.model.dto.response.AuthenticationResponse;

public interface IAuthService {
    AuthenticationResponse registration(RegistrationRequest request);
    AuthenticationResponse authentication(AuthenticationRequest authenticationRequest);
    AuthenticationResponse refreshToken(String authHeader);
}
