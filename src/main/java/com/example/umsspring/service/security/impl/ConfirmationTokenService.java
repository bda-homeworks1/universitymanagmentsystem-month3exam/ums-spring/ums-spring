package com.example.umsspring.service.security.impl;


import com.example.umsspring.exception.ApplicationException;
import com.example.umsspring.model.dto.response.ResponseDto;
import com.example.umsspring.model.entity.ConfirmationTokenEntity;
import com.example.umsspring.model.enums.Exceptions;
import com.example.umsspring.repository.ConfirmationTokenRepository;
import com.example.umsspring.service.security.IConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ConfirmationTokenService implements IConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Override
    public ResponseDto save(ConfirmationTokenEntity confirmationTokenEntity) {
        ConfirmationTokenEntity save = confirmationTokenRepository.save(confirmationTokenEntity);
        if (save != null){
            return new ResponseDto("Save is successfull");
        } else {
            throw new ApplicationException(Exceptions.TOKEN_IS_INVALID_EXCEPTION);
        }
    }

    @Override
    public ConfirmationTokenEntity getTokenByUUID(String uuid) {
        return confirmationTokenRepository.findConfirmationTokenByToken(uuid)
                .orElseThrow(() -> new ApplicationException(Exceptions.TOKEN_NOT_FOUND_EXCEPTION));
    }
}