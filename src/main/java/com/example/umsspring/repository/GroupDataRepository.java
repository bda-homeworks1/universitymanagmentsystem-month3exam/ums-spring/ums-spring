package com.example.umsspring.repository;

import com.example.umsspring.model.entity.GroupDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupDataRepository extends JpaRepository<GroupDataEntity, Long> {
}
