package com.example.umsspring.repository;

import com.example.umsspring.model.entity.JournalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalDataRepository extends JpaRepository<JournalDataEntity, Long> {
}
