package com.example.umsspring.repository;

import com.example.umsspring.model.entity.TokenEntity;
import com.example.umsspring.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TokenEntity, Long> {
    Optional<TokenEntity> findTokenByToken(String token);
    Optional<TokenEntity> findTokenByUser(UserEntity user);
}
