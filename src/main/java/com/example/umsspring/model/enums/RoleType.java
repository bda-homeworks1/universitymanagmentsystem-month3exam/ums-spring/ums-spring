package com.example.umsspring.model.enums;

public enum RoleType {
    ADMIN("ADMIN"),
    TEACHER("TEACHER"),
    STUDENT("STUDENT");

    public final String value;

    RoleType(String value) {
        this.value = value;
    }
}
